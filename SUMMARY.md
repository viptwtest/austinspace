# Table of contents

* [🧿 AustinSpace 筆記手札](README.md)
  * [🌌 動態](austinspace-bi-ji-shou-zha/dong-tai.md)
  * [🍥 簡歷](austinspace-bi-ji-shou-zha/jian-li.md)
  * [💎 資源分享](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/README.md)
    * [🧩 瀏覽器擴展](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/liu-lan-qi-kuo-zhan/README.md)
      * [🎯 二管家](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/liu-lan-qi-kuo-zhan/er-guan-jia.md)
    * [🖥 Windows](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/windows/README.md)
      * [📱 Deepl 翻譯](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/windows/deepl-fan-yi/README.md)
        * [windows app](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/windows/deepl-fan-yi/windows-app.md)
    * [🛍 Android](austinspace-bi-ji-shou-zha/zi-yuan-fen-xiang/android.md)
  * [📕 筆記](austinspace-bi-ji-shou-zha/bi-ji.md)
  * [📘 隱私政策](austinspace-bi-ji-shou-zha/yin-si-zheng-ce.md)
