---
description: 快捷導覽
cover: .gitbook/assets/MyLogoArt20231121211053.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: false
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: false
---

# 🧿 AustinSpace 筆記手札

{% hint style="info" %}

{% endhint %}

{% tabs %}
{% tab title="起始頁" %}
<table data-view="cards"><thead><tr><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><a href="http://www.bitsoo.cn/">比特導航</a></td><td></td><td></td><td><a href=".gitbook/assets/logo@2x.png">logo@2x.png</a></td><td><a href="http://www.bitsoo.cn/">http://www.bitsoo.cn/</a></td></tr><tr><td><a href="http://hao.shejidaren.com/index.html">設計導航</a></td><td></td><td></td><td><a href=".gitbook/assets/logo.png">logo.png</a></td><td><a href="http://hao.shejidaren.com/index.html">http://hao.shejidaren.com/index.html</a></td></tr><tr><td><a href="https://www.one-tab.com/page/JiFUav8zRjq270EchUPnfA">設計導航</a></td><td></td><td></td><td><a href=".gitbook/assets/file.excalidraw (1).svg">file.excalidraw (1).svg</a></td><td></td></tr><tr><td>     <a href="https://web.metadesk.cc/">MetaDeska 啟始頁</a></td><td></td><td></td><td><a href=".gitbook/assets/icons8-home-94.png">icons8-home-94.png</a></td><td></td></tr><tr><td><a href="https://ctool.dev/">開發工具箱</a></td><td></td><td></td><td><a href=".gitbook/assets/1698854409365.png">1698854409365.png</a></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td><td></td></tr></tbody></table>


{% endtab %}

{% tab title="學習" %}
<table data-view="cards"><thead><tr><th></th><th></th><th></th><th data-hidden data-card-cover data-type="files"></th></tr></thead><tbody><tr><td><a href="https://www.youtube.com/playlist?list=PLteWjpkbvj7pbdPXhIqgtX3v3pQxHz-5l">Odoo詳細教學</a></td><td></td><td></td><td><a href=".gitbook/assets/odoo-logo-main-banner (1).jpeg">odoo-logo-main-banner (1).jpeg</a></td></tr><tr><td></td><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr></tbody></table>


{% endtab %}

{% tab title="精選站點" %}

{% endtab %}

{% tab title="我的" %}


<div align="left" data-full-width="true">

<figure><img src=".gitbook/assets/icons8-group-94.png" alt=""><figcaption><p><a href="https://groups.google.com/my-groups?pli=1">Google 網路論壇</a></p></figcaption></figure>

</div>
{% endtab %}

{% tab title="工具" %}

{% endtab %}

{% tab title="AI" %}
<div align="left">

<figure><img src="https://www.aixuanfeng.com/wp-content/uploads/2023/08/ai%E6%97%8B%E9%A3%8E.png" alt="" width="188"><figcaption><p><a href="https://www.aixuanfeng.com/">AI 網站集合</a></p></figcaption></figure>

</div>
{% endtab %}

{% tab title="工作階段" %}
<div>

<figure><img src=".gitbook/assets/下載.png" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/下載 (1).png" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/下載 (2).png" alt=""><figcaption></figcaption></figure>

 

<figure><img src=".gitbook/assets/下載 (3).png" alt=""><figcaption></figcaption></figure>

</div>
{% endtab %}
{% endtabs %}

***

##

***
