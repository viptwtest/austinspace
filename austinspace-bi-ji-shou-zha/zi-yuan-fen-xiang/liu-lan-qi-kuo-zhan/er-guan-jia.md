---
description: 科技、資訊、便捷！都在這裡。
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 🎯 二管家

推薦擴展

<div align="left">

<figure><img src="../../../.gitbook/assets/unnamed.jpg" alt=""><figcaption><p><strong>二管家</strong></p></figcaption></figure>

</div>

擴展管理套件  → [下載地址](https://chrome.google.com/webstore/detail/nooboss/aajodjghehmlpahhboidcpfjcncmcklf) &#x20;

描述：

{% code title="一個全能的擴充功能管理" overflow="wrap" fullWidth="true" %}
```
一個管理擴充功能用的工具，可使用擴充功能更新通知並記錄擴充功能使用歷程，自動開啟/關閉擴充功能，根據目前網站取得二管家社群推薦
擴充功能管理的最終利器，記錄擴充功能歷程，自動開啟/關閉擴充功能，根據目前網站取得二管家社群推薦
通過二管家，你可以看到每個應用/擴充功能的詳細資訊，不管是它們需要的權限還是它們什麽時候更新的。
你也可以檢視二管家社群幫他們打了什麽標籤，這樣你就可以知道它們好不好用。你還可以設定自動規則，這樣擴充功能就只會在你開啟特定網頁的時候才開啟，其他時候都會關閉。

二管家提供的功能有：
*管理你的應用
---開啟/關閉/刪除一個或多個應用
*社群分享
---根據目前網頁看到二管家社群推薦的適用於目前網頁的擴充功能
---每個人都可以幫任何一個網站推薦好的擴充功能
---每個人都可以幫任何一個擴充功能打標籤
*自動套用狀態管理
---根據設定的規則自動啟用或停用應用
---(減少記憶體占用)
---(只有在需要的時候才開啟應用)
*應用歷程記錄
---記錄應用的安裝，移除，開啟，和關閉
---可以知道版本變化
*顯示應用詳細資訊
---下載crx檔案
---開啟manifest檔案
---檢視權限
---和各種各樣的詳細資訊
```
{% endcode %}
