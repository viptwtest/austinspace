---
description: 具有截圖翻譯Ocr功能的翻譯軟體。
cover: https://static.deepl.com/img/logo/deepl-logo-text-blue.svg
coverY: 0
layout:
  cover:
    visible: true
    size: full
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: false
---

# 📱 Deepl 翻譯

<div data-full-width="true">

<figure><img src="../../../../.gitbook/assets/image.png" alt=""><figcaption><p><mark style="color:yellow;"><strong>Deepl 翻譯是一個在線翻譯服務，它可以將文本或語音從一種語言翻譯成另一種語言。Deepl 翻譯使用了人工智能和深度學習技術，以提供高質量和準確的翻譯結果。Deepl 翻譯支持多種語言，包括英語、中文、日語、法語、德語等。Deepl 翻譯的優勢是它可以保留原文的語境和風格，並且可以處理複雜和專業的詞彙。Deepl 翻譯還可以根據用戶的偏好和需求，提供不同的翻譯選項和建議。</strong></mark></p></figcaption></figure>

</div>

## [官方網站下載連結](https://www.deepl.com/zh/windows-app/)
