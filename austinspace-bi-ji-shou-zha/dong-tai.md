---
description: 科技、資訊、便捷！都在這裡。
cover: ../.gitbook/assets/稿定设计-9.png
coverY: 0
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 🌌 動態

***

{% embed url="https://learn.microsoft.com/zh-tw/training/modules/use-docker-container-dev-env-vs-code/" %}





<figure><img src="../.gitbook/assets/2.png" alt=""><figcaption></figcaption></figure>

<mark style="color:blue;">**提供許多方便的應用及擴充，大致分為以下幾類。**</mark>







* 瀏覽器擴充
* 導航網站
* 工具網站
* 架站網站
* Hosting
* 影像設計



####

