---
description: 記錄我的學習
---

# 📕 筆記

2023/10/24 AM 12:10

<div align="left">

<figure><img src="https://learn.microsoft.com/en-us/training/achievements/use-docker-container-dev-env-vs-code.svg" alt=""><figcaption></figcaption></figure>

</div>

## 使用 Visual Studio Code 搭配 Docker 容器作為開發環境



必要安裝

* Visual Studio Code
* VS Code 的擴展 Visual Studio Code Dev Containers
* WSL 後端
* &#x20;Docker Desktop 2.0+



讓我們開始吧

首先以系統管理員身分執行 Windows Power Shell

輸入

```
wsl --install
```

完成後重新開機













<div align="left">

<figure><img src="https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE1Mu3b?ver=5c31" alt=""><figcaption><p>來源：Microsoft leam </p></figcaption></figure>

</div>
