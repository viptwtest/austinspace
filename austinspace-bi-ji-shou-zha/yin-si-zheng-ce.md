---
description: >-
  歡迎您訪問「AstinSpace網路手札」（以下簡稱本網站），本網站尊重並保護您的個人資料，請您詳細閱讀本隱私政策，以瞭解本網站如何蒐集、處理和保護您的個人資料
---

# 📘 隱私政策

一、適用範圍

本隱私政策適用於您在本網站使用的所有服務和功能，但不適用於本網站連結的其他網站或非本網站所委託或管理的人員。

二、個人資料的蒐集和使用

當您訪問本網站或使用本網站提供的服務時，我們可能會請您提供必要的個人資料，例如姓名、電子郵件、聯絡方式等，並在特定目的範圍內處理和利用您的個人資料。除非得到您的同意或依法律規定，我們不會將您的個人資料用於其他用途。

本網站也會記錄您使用本網站的相關資訊，例如IP位址、瀏覽器類型、訪問時間、瀏覽和點選記錄等，這些資訊僅供本網站內部分析和改善服務品質之用，不會對外公開或提供給第三方。

三、Cookie的使用

為了提供您更好的使用體驗，本網站會在您的電腦或行動裝置上儲存和讀取Cookie，Cookie是一種紀錄您在本網站的偏好和活動的小型文字檔案。如果您不願接受Cookie，您可以在您的瀏覽器設定中拒絕或刪除Cookie，但這可能會影響本網站部分功能的正常運作。

四、個人資料的保護

本網站採取適當的技術和組織措施來保護您的個人資料，防止未經授權的存取、使用、修改或洩漏。只有經過授權的人員才能接觸您的個人資料，並且都有保密義務。如果發生任何違反保密義務或危害個人資料安全的事件，我們將採取必要的應變措施並通知相關當事人。

如果因業務需要而必須委託其他單位提供服務時，我們也會嚴格要求其遵守保密義務，並且採取必要檢查程序以確保其將確實遵守。

五、個人資料的共用

本網站不會出售、交換或出租任何您的個人資料給其他個人、團體或私人企業，但有以下情況除外：

* 獲得您的書面同意。
* 依法律規定或應司法機關之要求。
* 為保護本網站或其他使用者的權益或財產。
* 為維護公共利益或社會安全。

六、個人資料的查詢、更正和刪除

您可以隨時向本網站提出查詢、更正或刪除您的個人資料，我們將儘快處理您的要求。但請注意，某些個人資料可能因法律規定或業務需要而無法刪除。

七、隱私政策的修改

本網站保留隨時修改本隱私政策的權利，修改後的條款將刊登於本網站上，並自公告之日起生效。建議您定期查閱本隱私政策，以瞭解最新的資訊。

八、聯絡我們

如果您對於本隱私政策或您的個人資料有任何問題或建議，歡迎隨時與我們聯絡，我們將盡力回答您的問題並改善我們的服務。的問題並改善我們的服務。



<div align="center" data-full-width="false">

<figure><img src="../.gitbook/assets/稿定设计-9.png" alt="AustinSpace"><figcaption></figcaption></figure>

</div>

<table data-view="cards"><thead><tr><th></th><th data-type="rating" data-max="4"></th><th data-type="users" data-multiple></th><th data-type="users" data-multiple></th><th></th><th></th><th data-type="select"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-card-cover data-type="files"></th></tr></thead><tbody><tr><td></td><td>4</td><td></td><td></td><td></td><td></td><td></td><td><a href="http://127.0.0.1:5000/o/Jp5DyVXxz8MI3lh5LnYd/s/PKgPD5JJJLjUnO16jTKF/">AstinSpace網路手札</a></td><td><a href="../.gitbook/assets/稿定设计-4.png">稿定设计-4.png</a></td></tr><tr><td></td><td>4</td><td></td><td></td><td></td><td></td><td></td><td><a href="http://127.0.0.1:5000/o/Jp5DyVXxz8MI3lh5LnYd/s/PKgPD5JJJLjUnO16jTKF/">AstinSpace網路手札</a></td><td><a href="../.gitbook/assets/稿定设计-9.png">稿定设计-9.png</a></td></tr><tr><td></td><td>4</td><td></td><td></td><td></td><td></td><td></td><td><a href="http://127.0.0.1:5000/o/Jp5DyVXxz8MI3lh5LnYd/s/PKgPD5JJJLjUnO16jTKF/">AstinSpace網路手札</a></td><td><a href="../.gitbook/assets/稿定设计-8.png">稿定设计-8.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="../.gitbook/assets/稿定设计-6.png">稿定设计-6.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="../.gitbook/assets/稿定设计-5.png">稿定设计-5.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td><a href="http://127.0.0.1:5000/o/Jp5DyVXxz8MI3lh5LnYd/s/PKgPD5JJJLjUnO16jTKF/">AstinSpace網路手札</a></td><td><a href="../.gitbook/assets/icons8-book-64.png">icons8-book-64.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="../.gitbook/assets/2.png">2.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="../.gitbook/assets/1.png">1.png</a></td></tr><tr><td></td><td>null</td><td></td><td></td><td></td><td></td><td></td><td></td><td><a href="../.gitbook/assets/1.png">1.png</a></td></tr></tbody></table>
